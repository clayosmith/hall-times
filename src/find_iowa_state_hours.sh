#!/bin/bash

IFS=$","
export LC_ALL=C
# declare -a buildings=( $(curl -s https://www.fpm.iastate.edu/maps/buildings/ | sed -n '/var buildingNames = \[/,/];/p' | sed '1d;$d' | tr -d '"' | tr ' ' '+') )
URL="https://www.fpm.iastate.edu/maps/buildings/building.asp?building="

: > ./resources/all_buildings.txt
curl -s https://www.fpm.iastate.edu/maps/buildings/ | 
	sed -n '/var buildingNames = \[/,/];/p' | 
	sed '1d;$d' | 
	tr -d '"' | 
	tr ' ' '+' | 
	tr ',' '\n' | 
	parallel --keep-order curl -s $URL{} |
	grep "<tr><th>Monday</th><td>\|mt-3" | 
	grep -o ">.*<" | 
	sed -e "s/\<tr\>/ /g" -e "s/\<\/*td\>//g" -e "s/\<\/*th\>/ /g" | 
	tr -d '<>-' |
	sed -e "s/^[ \t]*//" -e "s/Monday/Mon/" -e "s/Tuesday/Tue/" -e "s/Wednesday/Wed/" -e "s/Thursday/Thu/" -e "s/Friday/Fri/" -e "s/Saturday/Sat/" -e "s/Sunday/Sun/" -e "s/://g" -e "s/ AM/AM/g" -e "s/ PM/PM/g" -e "s/  / /g" -e "s/Midnight/12AM/g" -e "s/Unlocked 24 hours/24-HOURS/g" -e "s/Noon/12PM/g" | tee ./resources/all_buildings.txt

: > ./resources/just_buildings_with_hours.txt
grep -B1 "Mon " ./resources/all_buildings.txt | grep -v -- "^--$" > ./resources/just_buildings_with_hours.txt
