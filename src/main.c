#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>


/* NOTES
   	- means not open, @ means open
	96 dashes, 1 hour = 4 dashes, every 15 minutes = 1 dash
	First dash means that the building is open at midnight
	Last dash means that the building is open at 11:45 (if it closes at 11:45 do not put an '@')
	Dont dash the time that it closes if it falls in line with a dash
	------------------------------------------------------------------------------------------------
	0   1   2   3   4   5   6   7   8   9   ^   ^   0   1   2   3   4   5   6   7   8   9   ^   ^   0

	TODO need to know if it is the opening time or the closing time
		if i = -1 then its the start time multiply it by -1, if its 1 then its the second time
	TODO need to take the start time and open time and create a string
		need to handle if it is open until morning the next day to modify the next day's strings and to not later overwrite it
	TODO if open_time_index and/or close_time_index = -1 then the building is closed all day
	TODO function that gives you waht the next day is, including having Sunday wrap around to Monday
	TODO have day strings default to all dashes and then only replace what you need with '@' symbols in a function that takes a start time and end time (index?) and the day of the week to modify
*/


#define BUFFER_SIZE	1024
#define DAY_STRING_SIZE	97 //96 dashes and null terminating character
#define DAYS_IN_WEEK	7


struct building_list_node {
	char building_name[BUFFER_SIZE];
	char weekdays[DAYS_IN_WEEK][DAY_STRING_SIZE];
	struct building_list_node* next;
};


//create_node() {{{
struct building_list_node* create_node(struct building_list_node* current_node)
{
	if (current_node == NULL){//first node
		struct building_list_node* new_node = (struct building_list_node*) malloc(sizeof(struct building_list_node));
		new_node->building_name[0] = '\0';
		for (int i = 0; i < DAYS_IN_WEEK; ++i) {
			strncpy(new_node->weekdays[i], "------------------------------------------------------------------------------------------------", sizeof(char) * DAY_STRING_SIZE);
		}
		new_node->next = NULL;
		return new_node;
	} else {//not first node
		current_node->next = (struct building_list_node*) malloc(sizeof(struct building_list_node));
		//set default values
		current_node->next->building_name[0] = '\0';
		for (int i = 0; i < DAYS_IN_WEEK; ++i) {
			strncpy(current_node->next->weekdays[i], "------------------------------------------------------------------------------------------------", sizeof(char) * DAY_STRING_SIZE);
		}
		current_node->next->next = NULL;
		return current_node->next;
	}
}
//}}}


//next_day_index(){{{
int next_day_index(int current_day_index) {
	if (current_day_index == 6) {
		return 0;
	} else {
		return ++current_day_index;
	}
}
//}}}


//write_building_string(){{{
void write_building_string(struct building_list_node* node, int current_day_index, int start_index, int end_index) {
	if ( end_index < start_index) { //open until the next day
		int next_day = next_day_index(current_day_index);
		for (int i = 0; i < end_index; ++i) {
			node->weekdays[next_day][i] = '@';
		}
		for (int i = start_index; i < DAY_STRING_SIZE - 1; ++i) {
			node->weekdays[current_day_index][i] = '@';
		}
	} else {
		for (int i = start_index; i < end_index; ++i) {
			node->weekdays[current_day_index][i] = '@';
		}
	}
}
//}}}


//get_index() {{{
//NOTE 3:15 should be index 61
int get_index(char* str, char* option) {
	int include = 0;
	/* if (strstr(option, "open") != NULL) { */
	/* 	include = 0; */
	/* } else if (strstr(option, "close") != NULL) { */
	/* 	include = -1; */
	/* } else { */
	/* 	fprintf(stderr, "ERROR: inproper string passed in as second argument!\n"); */
	/* 	exit(-1); */
	/* } */
	int string_len = strlen(str);
	int time = 0, minutes = 0;
	switch (string_len) {
		case 3: //3PM
			time = (str[0] - '0') * 100;
			if (str[1] == 'P') {
				time += 1200;
			}
			break;
		case 4: //12PM //TODO 12AM and 12PM exceptions
			if (str[1] == '2') {
				if (str[2] == 'A') {
					time = 0;
				} else {
					time = 1200;
				}
			} else {
				time = 1000;
				time += (str[1] - '0') * 100;
				if (str[2] == 'P') {
					time += 1200;
				}
			}
			break;
		case 5: //445AM
			time = (str[0] - '0') * 100;
			minutes = (str[1] - '0') * 10;
			minutes += str[2] - '0';
			time += (minutes / 15) * 25;
			if (str[3] == 'P') {
				time += 1200;
			}
			break;
		case 6: //1120AM
			time = 1000;
			time += (str[1] - '0') * 100;
			minutes = (str[2] - '0') * 10;
			minutes += str[3] - '0';
			time += (minutes / 15) * 25;
			if (str[4] == 'P') {
				time += 1200;
			}
			break;
		default: {
			fprintf(stderr, "ERROR: INVALID STRING!\n");
			exit(-1);
			 }
	}
	return (time / 25) + include;
}
//}}}


//TODO function that returns the index of the string, it has to know if its the opening or closing time so it know to go up to the nth index or nth - 1 index as is the case for closing
//TODO after running the function twice, if the index of the closing call is smaller then the first call to the function then that means that the day wrapped around so set the index of the first day to the last dash and then set the next day from 0 to the index given back
//TODO make function for getting the next day (SUNDAY wraps around to MONDAY)
//TODO function two take two indexes and writes new string to list element
//parse_time(){{{
void parse_time(struct building_list_node* list, char* string) {
	//strlen 3 = Mon or 1PM
	//strlen 4 = 11PM
	//strlen 5 = 730PM
	//strlen 6 = 1030PM or Locked
	//strlen 8 = 24-HOURS
	//if the first character is a M T W F or S then increment the day
	//if the first letter is L then their is no time for the day
	//if the third character is a - then its open all day
	//if the first character is a number then combine with the next strtok to get the hours for the day TODO function that takes both strings seperately (so it can get their lengths and know how to interpret them)
	char* token;
	token = strtok(string, " \n");
	int weekday_index = -1;
	int length, length2;
	while (token != NULL) {
		if (token[2] == '-') { //open all day
			strncpy(list->weekdays[weekday_index], "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@", DAY_STRING_SIZE);
		} else if (token[0] == 'L') { //Locked
			//dont need to do anything as this is default
		} else if (token[0] >= '0' && token[0] <= '9') { //hours
			char* token2 = strtok(NULL, " \n");
			assert(token2 != NULL && "SHOULD NEVER BE A NULL POINTER BECAUSE THE FIRST NUMBERED INPUT SHOULD BE FOLLOWED B ANOTHER TIME GIVEN WITH NUMBERS");
			int start_index = get_index(token, "open");
			int close_index = get_index(token2, "close");
			write_building_string(list, weekday_index, start_index, close_index);
		} else {
			switch (token[0]) {//new weekday
				case 'M':
				case 'T':
				case 'W':
				case 'F':
				case 'S':
					++weekday_index;
					break;
				default: {
					fprintf(stderr, "ERROR: Should never make it here!\n");
					exit(-1);
					  }
			}
		}
		token = strtok(NULL, " \n");
	}
}
//}}}


//print_list(){{{
void print_list(struct building_list_node* head) {
	struct building_list_node* current_node = head;
	while (current_node != NULL) {
		printf("%s"	\
				"Mon:%s\n"	\
				"Tue:%s\n"	\
				"Wed:%s\n"	\
				"Thu:%s\n"	\
				"Fri:%s\n"	\
				"Sat:%s\n"	\
				"Sun:%s\n"	\
 				"    0   1   2   3   4   5   6   7   8   9   ^   ^   0   1   2   3   4   5   6   7   8   9   ^   ^   0\n\n\n\n",
		      current_node->building_name, 
		      current_node->weekdays[0],
		      current_node->weekdays[1],
		      current_node->weekdays[2],
		      current_node->weekdays[3],
		      current_node->weekdays[4],
		      current_node->weekdays[5],
		      current_node->weekdays[6]
		      );
		current_node = current_node->next;
	}
}
//}}}


//main() {{{
int main(void)
{
	FILE* fptr = fopen("./resources/just_buildings_with_hours.txt","r");
	if (fptr == NULL) {
		perror("ERROR");
		exit(-1);
	}
	struct building_list_node* head_node = create_node(NULL);
	struct building_list_node* current_node = head_node;
	char buffer[BUFFER_SIZE];
	while (fgets(buffer, sizeof(char) * BUFFER_SIZE, fptr) != NULL) {
		if (strstr(buffer, "Mon ") != NULL && strstr(buffer, "Tue ") != NULL) { //just in case for some reason a building's name includes Mon and a space after it
			//parse time and add it to linked list
			parse_time(current_node, buffer);
		} else {
			//create list node and add building name to it
			if (current_node->building_name[0] != '\0') {//not the very first time and should already have filled this node, so I need to create a new one
				current_node = create_node(current_node);
			}
			strncpy(current_node->building_name, buffer, (sizeof(char) * BUFFER_SIZE) - 1);
		}
	}
	print_list(head_node);
	return 0;
}
//}}}
