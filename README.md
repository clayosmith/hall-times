# Hall Times



## Purpose

Prints the open hours for each of the buildings at Iowa State that provide hours on their website in an orderly fashion.

[text](https://www.clayosmith.com) after text
![example output](./resources/hall-times_screenshot.png)
