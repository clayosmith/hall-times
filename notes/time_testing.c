#include <stdio.h>
#include <time.h>


int main(void)
{
	time_t cur_time_sec;
	struct tm* time_values;
	char buffer[100];

	time(&cur_time_sec);

	time_values = localtime( &cur_time_sec);

	strftime(buffer, 100, "%A, %H:%M", time_values);		


	printf("Time is: %s\n", buffer);
	return 0;
}
